# searchJiraTasks

A simple script to search jira tasks for a specific query search string, retrieve and concatenate the response (pagination of 100 results) and saves as a csv file.

## How to run this script

create a file called `.env` and add set following variables:

```
HOST=
JIRA_USER=
JIRA_TOKEN=
```
then you're good to go and can simply run:

```
npm install
npm run start
```