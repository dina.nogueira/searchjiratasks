import axios, {AxiosResponse} from "axios";
import { parse } from "json2csv";
import fs from "fs";
import path from "path";

const { HOST, JIRA_TOKEN, JIRA_USER } = process.env;

const jiraTasksFile = path.join(
    __dirname,
    "jira_tasks.csv"
  );

interface JiraSearch {
    total: number,
    startAt: number,
    tickets: JiraTicket[]
}

interface JiraTicket {
    projectName: string,
    id: string,
    key: string,
    created: string,
    ticketTitle: string,
    parentTicket?: string,
    parentTicketName: string,
    estimationTime?: string,
    storyPointEstimate?: string,
    storyPoints?: string,
}

function mapJiraTicketInfo(ticket:any): JiraTicket {
    return {
        projectName: ticket?.fields?.project?.name,
        id: ticket?.id,
        key: ticket?.key,
        created: ticket?.fields?.created,
        ticketTitle: ticket?.fields?.summary,
        parentTicket: ticket?.fields?.parent?.key,
        parentTicketName: ticket?.fields?.parent?.fields?.summary,
        estimationTime: ticket?.fields?.timeestimate,
        storyPointEstimate: ticket?.fields?.customfield_10016,
        storyPoints: ticket?.fields?.customfield_10024,
    }
}

function fail(msg: string): never {
    throw new Error(msg);
  }

const jira = axios.create({
    baseURL: HOST || fail("missing HOST"),
    auth: {
    // create one api token here: https://id.atlassian.com/manage-profile/security/api-tokens
        username: JIRA_USER || fail("missing JIRA_USER"),
        password: JIRA_TOKEN || fail("missing JIRA_TOKEN"),
    },
    headers:{
        'Accept': 'application/json',
        'Content-Type':'application/json',
    },
    validateStatus: () => true,
  });

async function jiraPostSearch(startAt: number, searchString?: string, ): Promise<AxiosResponse> {
    return await jira.post("/search",{
        "jql": searchString,
        "startAt": startAt,
        "maxResults": 100
    }) 
}

async function searchJiraIssues(startAt: number, searchString?: string, ): Promise<JiraTicket[]> {
    let tickets: JiraTicket[] = []
    let result = await jiraPostSearch( startAt, searchString )
    console.log("start at", result.data.startAt)

    tickets = result.data.issues.map((ticket: any) => mapJiraTicketInfo(ticket))
    const jiraSearch: JiraSearch = {total: result.data.total, startAt: result.data.startAt, tickets}

    return jiraSearch.tickets
}

async function getTotalFromSearch(searchString: string): Promise<number> {
    let result = await jiraPostSearch( 0, searchString )
    return result.data.total
}

async function toCsv(data: JiraTicket[]){
    const fields = [
        "projectName",
        "id",
        "key",
        "created",
        "ticketTitle",
        "parentTicket",
        "parentTicketName",
        "estimationTime",
        "storyPointEstimate",
        "storyPoints",
    ]
    const opts = { fields };

    try {
        const csv = parse(data, opts);
        fs.writeFileSync(jiraTasksFile, csv);
    } catch (err) {
        console.error(err);
    }
}

async function listJiraIssues()  {
    let startAt = 0;
    const jql = "timeestimate > 0 OR 'Story Points' > '0' OR 'Story point estimate' > '0' AND created >= '2020/01/01'"
    const totalIssues: number = await getTotalFromSearch(jql)
    let data: JiraTicket[] = []

    while (startAt < totalIssues) {
        data = data.concat(await searchJiraIssues(startAt, jql))
        startAt+=100
    }
    toCsv(data)
}

listJiraIssues()